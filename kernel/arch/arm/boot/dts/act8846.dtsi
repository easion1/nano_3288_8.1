

&i2c0 {
        clock-frequency = <400000>;
        status = "okay";

        vdd_cpu: syr827@40 {
                compatible = "silergy,syr827";
                fcs,suspend-voltage-selector = <1>;
                reg = <0x40>;
                regulator-name = "vdd_cpu";
                regulator-min-microvolt = <850000>;
                regulator-max-microvolt = <1350000>;
                regulator-always-on;
                regulator-boot-on;
                regulator-enable-ramp-delay = <300>;
                regulator-ramp-delay = <8000>;
                vin-supply = <&vcc_sys>;
                regulator-state-mem {
                        regulator-off-in-suspend;
                };
        };

        vdd_gpu: syr828@41 {
                compatible = "silergy,syr828";
                fcs,suspend-voltage-selector = <1>;
                reg = <0x41>;
                regulator-name = "vdd_gpu";
                regulator-min-microvolt = <850000>;
                regulator-max-microvolt = <1350000>;
                regulator-always-on;
                regulator-ramp-delay = <6000>;
                vin-supply = <&vcc_sys>;
                regulator-state-mem {
                        regulator-off-in-suspend;
                };
        };
        act8846: act8846@5a {
                compatible = "active-semi,act8846";
                reg = <0x5a>;
                status = "okay";
                pinctrl-names = "default";
                //pinctrl-0 = <&pmic_vsel>, <&pmic_sleep>, <&pwr_hold>;
                pinctrl-0 = <&pmic_vsel>, <&pwr_hold>;
                system-power-controller;

                vp1-supply = <&vcc_sys>;
                vp2-supply = <&vcc_sys>;
                vp3-supply = <&vcc_sys>;
                vp4-supply = <&vcc_sys>;
                inl1-supply = <&vcc_io>;
                inl2-supply = <&vcc_sys>;
                inl3-supply = <&vcc_20>;

                regulators {
                        vcc_ddr: REG1 {
                                regulator-name = "VCC_DDR";
                                regulator-min-microvolt = <1200000>;
                                regulator-max-microvolt = <1200000>;
                                regulator-always-on;
                        };

                        vcc_io: REG2 {
                                regulator-name = "VCC_IO";
                                regulator-min-microvolt = <3300000>;
                                regulator-max-microvolt = <3300000>;
                                regulator-always-on;
                        };

                        vdd_log: REG3 {
                                regulator-name = "VDD_LOG";
                                regulator-min-microvolt = <1100000>;
                                regulator-max-microvolt = <1100000>;
                                regulator-always-on;
                        };

                        vcc_20: REG4 {
                                regulator-name = "VCC_20";
                                regulator-min-microvolt = <2000000>;
                                regulator-max-microvolt = <2000000>;
                                regulator-always-on;
                        };

                        vccio_sd: REG5 {
                                regulator-name = "VCCIO_SD";
                                regulator-min-microvolt = <1800000>;
                                regulator-max-microvolt = <3300000>;
                                regulator-always-on;
                        };

                        vdd10_lcd: REG6 {
                                regulator-name = "VDD10_LCD";
                                regulator-min-microvolt = <1000000>;
                                regulator-max-microvolt = <1000000>;
                                regulator-always-on;
                        };

                        vcca_codec: REG7 {
                                regulator-name = "VCCA_CODEC";
                                //regulator-min-microvolt = <1800000>;
                                //regulator-max-microvolt = <1800000>;
                                regulator-min-microvolt = <2800000>;
                                regulator-max-microvolt = <2800000>;
                                //regulator-max-microvolt = <3300000>;
                        };
                        vcca_tp: REG8 {
                                regulator-name = "VCCA_TP";
                                regulator-min-microvolt = <3300000>;
                                regulator-max-microvolt = <3300000>;
                                regulator-always-on;
                        };

                        vccio_pmu: REG9 {
                                regulator-name = "VCCIO_PMU";
                                regulator-min-microvolt = <3300000>;
                                regulator-max-microvolt = <3300000>;
                                regulator-always-on;
                        };

                        vdd_10: REG10 {
                                regulator-name = "VDD_10";
                                regulator-min-microvolt = <1000000>;
                                regulator-max-microvolt = <1000000>;
                                regulator-always-on;
                        };

                        vcc_18: REG11 {
                                regulator-name = "VCC_18";
                                regulator-min-microvolt = <1800000>;
                                regulator-max-microvolt = <1800000>;
                                regulator-always-on;
                        };

                        vcc18_lcd: REG12 {
                                regulator-name = "VCC18_LCD";
                                regulator-min-microvolt = <1800000>;
                                regulator-max-microvolt = <1800000>;
                                regulator-always-on;
                        };
                };
        };
};


&pinctrl {
        act8846 {
                pmic_vsel: pmic-vsel {
                        rockchip,pins = <7 10 RK_FUNC_GPIO &pcfg_output_low>;
                };
                pwr_hold: pwr-hold {
                        rockchip,pins = <0 10 RK_FUNC_GPIO &pcfg_output_high>;
                };
        };
};


&io_domains {
	status = "okay";
	sdcard-supply = <&vccio_sd>;
	wifi-supply = <&vcc_18>;
        audio-supply = <&vcca_codec>;
	bb-supply = <&vcc_io>;
	dvp-supply = <&vcc18_dvp>;
	flash0-supply = <&vcc_flash>;
        gpio30-supply = <&vccio_pmu>;
        gpio1830-supply = <&vcc_io>;
};
